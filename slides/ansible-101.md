---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->
![bg left:40% 100%](img/ansible.png)



## Ansible 101 

Concepts et principes de base

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)
# Présentation Générale

---

![w:200 center](img/ansible.png) est un outil d'**Automatisation de tâches IT**, et d'**Orchestation** d'infrastructure

- Logiciel libre, disponible sous licence GPL v3
- Créé en 2012 par Michael De Haan, édité depuis 2015 par RedHat (Groupe IBM)
- Développé en Python.

**Conçu pour être simple !!**

---

## Tâches IT 

- Gestion de serveurs
- Gestion de configuration
- Déploiement d'applications et de services
- Traitement de données
- ...

## L'Automatisation

- Réaliser des taches sans intervention humaine
- Gagner du temps, de la fiablité, de la sécurité

---

## L'orchestration

**Mise en place de Worflows complets de déploiment, configuration,  suivi, ...**

- Configuration d'OS :  Linux, windows, ...
- Configuration d'applications : Apache, Mysql, Hadoop, ...
- Déploiement de serveurs : machines Physiques (ipmi), Machine Virtuelle, Cloud, container (docker, LXC, ...)
- Stockage, réseau, Gestion d'identité, ...
- Interraction avec outils comme Jira, git, github, gitlab ,...
- ...

---
 
# Infrastructure as code : Pets vs Cattle

![h:400](img/pets_cattle.jpg)


---

# Atouts d'ansible

## Simple a installer

- Pas d'agent sur les serveurs à gérer
- Uniquement un accès SSH

## Simple à prendre en main

- Utilisation de fichier texte, lisibles, pas de code à développer
- Courbe d'apprentissage basse,
- Transcription très simples de commandes shell

---

## Simple à comprendre, et prévisible

- Les taches s'éxécutent les unes après les autres

## Robuste

- Pas de "master"

## Puissant

- Evolution progressive vers les fonctionnalités avancées
- Beaucoups d'exemples, de documentation
- Beaucoup de ressources directement utilisables
  
---

# L'idempotence

***Une opération a le même effet qu'on l'applique une ou plusieurs fois.***

Ansible garanti l'idempotence de l'ensemble de ses modules.

Une tache peut être lancée plusieurs fois :
- l’état final reste le même que lors du premier lancement.
- seules les actions nécessaires seront exécutées.

Par exemple, le module de creation de dossier ne créera le dossier que si il n'existe pas déjà.

---

# Inconvénients 

Mis à part IBM ...

- Parfois moins bien que ces concurents (CFEngine, Puppet, salt, Chef,Terraform, ...), mais couverture fonctionnelle plus complète
- Principe d'indépendance des taches pas toujours simple à gérer:  suivi de variables, ...
- Nécessite de repecter l'idempotence dans la définition de ses taches.
- Problème de performance si beaucoup d'hôtes à gérer : Mode push = goulet d'étranglement  (un mode Pull exite mais très peu utilisé)

---

# Ansible est "Tendance"

**Surtout pour des utilisations dans les domaines du Devops et du Cloud**
![h:500](img/gtrends-ansible.png)

--- 
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Architecture

---

# Vocabulaire

Listes de serveurs à gérer : **inventory**

Utilisation taches prêtes à l'emploi : **les modules**

Une execution simple de taches :

- soit en ligne de commande : **mode AdHoc**
- soit dans un fichier YAML permettant de regouper plusieurs taches : **les playbooks**

---
![height:600px](img/ansible-archi_simple.jpg)

---

# Pour aller un peu plus loin

Ansible sait s'appuyer sur des **templates** : jinja2

Les playbooks proposent de nombreuses fonctionnalités avancées :

- gestion de conditions,
- gestion de handlers (appelés pour relancer un service par exemple)

Pour regrouper des taches et fichiers de configuration dans **des Rôles** : permettent de 

- Approche fonctionelle
- Rapidité de configuration
- Rapidité de mise en oeuvre, robustesse
- Partage
  
---
# Et encore un peu plus loin 

Pour regroupes des Rôles, des playbook, ... : **les collections**

Protéger des données mots, de passe : **Ansible Vault**

Limiter l'execution à certaines taches en fonction du contexte : **les Tags**

Etendre les fonctionnalité d'Ansible pour ajouter de nouveaux moduels : **les plugins**

...

---
# Connexion SSH

Ansible s'appuis sur SSH. 

- authentification par Clé SSH ou par mot de passe
- possibilité de se demander un mot de passe pour les opérations necessitant des droits root.
- possibilité d'élévation des privilèges de l'utilisateur utilisé pour la connexion : **become**
- ...

**Si pas d'accès SSH, c'est qu'on a pas le droit de gérer la machine ...**

**Idéalement mettre en place une connexion ssh, avec clé SSH , et un utilisateur non root, avec des droits "sudo"**

---

# Pour résumer, pour utiliser Ansible 

1. Installer Ansible sur une machine de management le "Contoller-Node"
   - machine serveur ou poste de travail
   - pas de base de données, pas d'installation sur les postes clients, ...
   - nécessite Python
2. configurer hôtes avec lesquels on doit travailler : les "Managed nodes"
   - les déclarer dans l'inventory
   - configurer l'accès SSH depuis la machine de management
   - nécessite Python
3. Lancer des tâches, ou écrire (ou récupérer) des fichiers de taches.


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Installation

---
# Packages

## Packages éditeurs

Ansible est intégré dans les packages officiels de la plupart des distributions : Red Hat, CentOS, Fedora, Debian, Ubuntu, ... 

- Souvent en retard de plusieurs version
- Pas forcément l'ensemble des modules

## Packages "Ansible"

Ansible propose également ses propres dépots, avec des versions à jour.

---

## Python pip

Ansible est écrit en python, et est disponible sous forme d'un module installable avec "pip" (Python package manager)

## Code Source

Il est également possible de l'installer depuis le code source : [https://github.com/ansible/ansible](https://github.com/ansible/ansible)


---
<!-- _class: invert -->
<!-- _color: white -->

# Lancement de l'environnement de Lab 

🗹 Déployer l'environnement de lab, et le tester
🗹 se connecter au **Control-Node**

---

Récupérer l'environnement de Labs depuis le dépot (voir url dans la rubrique )


* Initaliser l'environnement : `make create`
* Lancer m'environnementdes labs :  `make start`
* Tester le bon fonctionnement de l'environnement : `make test`
* Accèder au Control-Node pour dérouler les labs : `make go`

Plus d'informations sur le site [https://ebraux.gitlab.io/ansible-labs/](https://ebraux.gitlab.io/ansible-labs/)

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "decouverte"  

🗹 Lancer l'environnement de lab et se connecter au **Control-Node**
🗹 Tester le modèle Pet vs Cattle

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Configuration

---

# Configuration du comportement d'Ansible

##  ansible.cfg

- par défaut : /etc/ansible/ansible.cfg
- configuration de la connexion SSH
- chemins par défaut (playbooks, rôles, ...)
- ...

---

# Liste des machines cible
 
## Inventory 

- par défaut : /etc/ansible/hosts
- Utilisation Basique : 
    - fichier text, en YAML ou Json
    - structuration en groupes de machines (Production/Test, Frontend/Backend, ...)
- Utilisation avancée : inventaire dynamique 
    - ecrit en pyhton, ou autre
    - plugins disponibles pour Openstack, AWS, Azure, VSphere, ...

--- 
## Exemple de fichier inventory

```ini
[prod:children]
frontend-prod
backend-prod

[frontend-prod]
srv-front-01
srv-front-02

[backend-prod]
srv-back-01

[rec]
srv-rec-01

```


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Les modules

---

# Les Modules

Les taches executées avec ansible s'appuient sur des modules.

Un module apporte des fonctionnalités, des ations qui pourront être lancées sur les Managed-Nodes.

Les plus communément utilisés :
- apt / yum : gérer des package (installer, supprimer, ...)
- file : gérer des fichiers et dossiers
- authorized_key : gérer les "SSH authorized key"
- setup : collecter des informations sur les Managed-Nodes
- ...

[https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html](https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html)


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Mode Ad-HOC
Lancement manuel de taches

---

# Mode Ad-HOC

## Utilisation d'ansible en ligne de commande.

Usage basique, pour faire des opérations exeptionelles :

- Arrêter/rebooter certains serveurs
- récupérer les informations sur un systéme
- ...
 
Toutes les "Ad-Hoc command" peuvent être utilisée dans un playbook.

Et dans la plupart des cas, autant faire un playbook basique.

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "adhoc" : Configuration d'Ansible et utilisation de commandes adHoc

🗹 Réaliser une configuration minimum d'Ansible
🗹 Tester l'accès aux **Managed-Nodes**

---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Les playbooks

---

# Les playbooks

## Si vous devez executer des taches plus d'une fois, ecrivez un playbook !!

Descriptions des tâches dans un fichier.

2 sections :

- **targets section** : les Managed-node qui vont être traités
- **tasks section**   : liste de  taches a executer (dans l'ordre)

Fichier au format YAML = c'est du texte, donc facilement versionnable
 
---

# Quelques notions de YAML

- Les fichiers démarrent avec `---`
- L'indentation est importante
- Variables, Dictionnaire (clé/valeur), et listes :
```yaml
  - martin:
      name: Martin D'vloper
      job: Developer
      skills:
        - python
        - perl
        - pascal
```
---

# Exemple de playbook

Installer le package "fail2ban" sur tous les "Managed-nodes" : 
```yaml
- hosts: all

  tasks:
  - apt:
      name: fail2ban
      state: latest
```

---
# Pour améliorer les playbook

- Utiliser des variables
- Nommer les choses.

```yaml
- name: Playbook de demo apt
  hosts: all

  vars:
    package_name: fail2ban

  tasks:
  - name: Installer un package avec le module apt
    apt:
      name: "{{ package_name }}"
      state: present
```

---

# Autres options

- Adapter/configurer l'execution par Ansible
```yaml
- name: update web servers
  hosts: webservers
  remote_user: ubuntu
  become: yes
```
- Inclure de multiples playbooks dans un playbook principal
```yaml
  - import_playbook: config_users.yml
  - import_playbook: fail2ban.yml
```
- ...
  
---

<!-- _class: invert -->
<!-- _color: white -->

# LAB "playbook" : Utiliser des playbooks

🗹 Lancer des playbooks
🗹 Tester des fonctionnalités

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Templates

Jinja2

---
# Utilisation de Jinja2

- Remplacement de variables
- instructions avancées : "for", "if/then/else", ...
- commentaires
- des filtres de transformation de contenu
- ...


---

# Exemple de tempate

Fichier de template : 

```jinja2
{% for user in users %}
   login={{ user.login }}, email={{ user.email }}
{% endfor %}
```

Utilisation : 
```yaml
- name: Template a file to /etc/userlist.conf
  template:
    src: templates/userlist.j2
    dest: /etc/userlist.conf
```

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "template" : Les templates

🗹 Génerer un fichier de configuration

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Les Variables


---
# Type de variables

- Variables simples
- listes
- dictionnaires
  
Rem : il est possible d'appliquer des filtres JinJa2 (upper, lower, ...)

---

# Définition des variables 

Les variables peuvent être définies :

- Dans un playbook 
- Dans un rôle
- Dans l'inventory : 
  - directement dans le fichier principal 
  - dans une arborescence structurée en "group_vars" et "host_vars"
- Via la ligne de commande
- Pendant l'éxecution d'une tâche

**L'ordre de priorité est important !!**

---
# Les Ansible Facts

Ansible collecte des information sur les Managed-Nodes

- systeme d'exploitation
- configuration réseau
- configuration materielle
- ...

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "variable" : Les variables

🗹 Créer différent type de variables
🗹 Tester l'ordre de priorité
🗹 Utiliser des Facts

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Les Rôles

---

# ANSIBLE ROLES

Rassembler dans un package taches variables, playbooks, fichiers, templates, ...

Orienté fonctionnel : gestion d'un logiciel (installer, configurer, ... )

Interêt :
- capitalisation : ré-utitiliser le rôle dans plusieurs playbooks
- partage

Le fonctionnement du rôle doit pouvoir êter adapté avec des variables.

---

# Structure d'un rôle

Un rôle est structuré en dossier: 

- tasks : les taches,
- handler : les actions sur notification
- files : fichiers
- template :  les templates au format Jinja2
- vars : variables
- defaults : valeurs par défaut 

Chaque dossier contient un fichier 'main.yml', utilisé par défaut (sauf pour tempates)

 

---

# Utilisation d'un rôle

```yaml
---
- name: Securisation des managed- node
  hosts: all
  roles:
    - hardening
```

---
# Partage d'un rôle : Ansible-galaxy

- hub qui permet de publier et diffuser des rôles
- mis à la disposition de la communauté par ansible
- intégré à ansible (commande `ansible-galaxy`)
- indexation par tags, score qualité, popularité

Ansible galaxy [https://galaxy.ansible.com/](https://galaxy.ansible.com/)


*Les rôles publiés peuvent être utilisés directement ou servir d'inspiration pour réaliser vos premiers rôles.*

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "role" : Utiliser des rôles

🗹 Utiliser et observer un rôle basique
🗹 Utiliser et observer un rôle d'Ansible Galaxy


---
<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

# Structures de contrôle

Boucles, conditions, ...

---

# Les boucles

Permet d'éxecuter une tache plusieurs fois.
Par exemple pour installer plusieurs packages :

```yaml
tasks:
  - name: install several package
    apt:
      name: "{{ item }}"
      state: latest
    loop:
       - fail2ban
       - rsync
```
*Anciennement `with_items`*

---

# Les conditions

Pour n'executer une tache que si necessaire

Example
```yaml
tasks:
  - name: "shut down Debian flavored systems"
    command: /sbin/shutdown -t now
    when: ansible_facts['os_family'] == "Debian"
```

---
# Blocks

Regrouper des taches, par exemple pour améliorer la gestion des erreurs(try/catch)

Example
```yaml
tasks:
 - name: Handle the error
   block:
     - debug:
         msg: 'I execute normally'
     - name: i force a failure
       command: /bin/false
     - debug:
         msg: 'I never execute, due to the above task failing, :-('
   rescue:
     - debug:
         msg: 'I caught an error, can do stuff here to fix it, :-)'
```

---
<!-- _class: invert -->
<!-- _color: white -->

# LAB "control" : Les structures de contrôle

🗹 Tester les diffrentes structures de contrôle

---

<!-- _backgroundColor: #5bbdbf -->
<!-- _color: white -->

![bg left:30% 80%](img/ansible.png)

#  https://docs.ansible.com/


